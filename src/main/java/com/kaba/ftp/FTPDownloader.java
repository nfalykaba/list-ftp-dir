package com.kaba.ftp;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPClientConfig;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPHTTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPSClient;
import org.apache.commons.net.util.TrustManagerUtils;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kaba
 */
public class FTPDownloader {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(FTPDownloader.class);

    private boolean binaryTransfer = false, error = false;
    private boolean localActive = false;
    private long keepAliveTimeout = -1;
    private int controlKeepAliveReplyTimeout = -1;

    private String protocol; // SSL protocol
    private String trustmgr;

    private String proxyHost;
    private int proxyPort = 80;
    private String proxyUser;
    private String proxyPassword;

    private String host;
    private Integer port = 21;
    private String username;
    private String password;
    private boolean useEpsvWithIPv4 = true;

    private String encoding;
    private String serverType;
    private String remoteDirectory;

    private FTPClient ftp = null;

    public FTPDownloader() {
    }

    /**
     * Open connection
     *
     * @throws IOException
     */
    public void connect() throws IOException {
        if (isConnected()) {
            close();
        }
        initConnection();
    }

    private void initConnection() {

        Preconditions.checkState(!Strings.isNullOrEmpty(remoteDirectory), "Remote (FTP) directory is null");

        if (Strings.isNullOrEmpty(protocol)) {
            if (!Strings.isNullOrEmpty(proxyHost)) {
                LOGGER.debug("Using HTTP proxy server: {} ", proxyHost);
                ftp = new FTPHTTPClient(proxyHost, proxyPort, proxyUser, proxyPassword);
            } else {
                ftp = new FTPClient();
            }
        } else {
            FTPSClient ftps;
            if (protocol.equals("true")) {
                ftps = new FTPSClient(true);
            } else if (protocol.equals("false")) {
                ftps = new FTPSClient(false);
            } else {
                String prot[] = protocol.split(",");
                if (prot.length == 1) { // Just protocol
                    ftps = new FTPSClient(protocol);
                } else { // protocol,true|false
                    ftps = new FTPSClient(prot[0], Boolean.parseBoolean(prot[1]));
                }
            }
            ftp = ftps;
            if ("all".equals(trustmgr)) {
                ftps.setTrustManager(TrustManagerUtils.getAcceptAllTrustManager());
            } else if ("valid".equals(trustmgr)) {
                ftps.setTrustManager(TrustManagerUtils.getValidateServerCertificateTrustManager());
            } else if ("none".equals(trustmgr)) {
                ftps.setTrustManager(null);
            }
        }
        if (keepAliveTimeout >= 0) {
            ftp.setControlKeepAliveTimeout(keepAliveTimeout);
        }
        if (controlKeepAliveReplyTimeout >= 0) {
            ftp.setControlKeepAliveReplyTimeout(controlKeepAliveReplyTimeout);
        }
        if (encoding != null) {
            ftp.setControlEncoding(encoding);
        }

        // suppress login details
        ftp.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out), true));

        final FTPClientConfig config;
        if (serverType != null) {
            config = new FTPClientConfig(serverType);
        } else {
            config = new FTPClientConfig();
        }
        ftp.configure(config);

        try {
            int reply;

            Preconditions.checkState(!Strings.isNullOrEmpty(host), "FTP host is null");
            port = port == 0 ? 21 : port;

            ftp.connect(host, port);

            LOGGER.debug("Connected to {} on {}", host, port);

            // After connection attempt, you should check the reply code to verify
            // success.
            reply = ftp.getReplyCode();

            if (!FTPReply.isPositiveCompletion(reply)) {
                ftp.disconnect();
                LOGGER.error("FTP server refused connection.");
                System.exit(1);
            }
            __main:
            if (!ftp.login(username, password)) {
                ftp.logout();
                error = true;
                break __main;
            }
            if (binaryTransfer) {
                ftp.setFileType(FTP.BINARY_FILE_TYPE);
            }
            ftp.enterLocalPassiveMode();

        } catch (IOException e) {
            if (ftp.isConnected()) {
                try {
                    ftp.disconnect();
                } catch (IOException f) {
                    // do nothing
                }
            }
            LOGGER.error("Could not connect to server.");
            e.printStackTrace();
        }
    }

    /**
     * Download a single directory from an FTP server.
     *
     * @param remoteCurrentDir Path of the current directory being downloaded.
     * @return List
     * @throws java.io.IOException
     */
    public List<String> listSingleDirectory(String remoteCurrentDir) throws IOException {

        this.connect();

        List<String> files = new ArrayList<>();

        FTPFile[] subFiles = ftp.listFiles(remoteDirectory + "/" + remoteCurrentDir);

        if (subFiles != null && subFiles.length > 0) {

            for (FTPFile aFile : subFiles) {

                String currentFileName = aFile.getName();
                if (currentFileName.equals(".") || currentFileName.equals("..")) {
                    // skip parent directory and the directory itself
                    continue;
                }
                // Build current remote file full path
                String filePath = remoteDirectory + "/" + remoteCurrentDir + "/"
                        + currentFileName;

                if (!aFile.isDirectory()) {
                    // download the file
                    LOGGER.info("DOWNLOADED the file: " + filePath);
                    files.add(currentFileName);
                }
            }
        }
        this.close();
        return files;
    }

    /**
     * Download a single file from the FTP server
     *
     * @param remoteFilePath path of the file on the server
     * @param savePath path of directory where the file will be stored
     * @return true if the file was downloaded successfully, false otherwise
     * @throws IOException if any network or IO error occurred.
     */
    public boolean downloadSingleFile(String remoteFilePath, String savePath) throws IOException {

        File downloadFile = new File(savePath);

        File parentDir = downloadFile.getParentFile();
        if (!parentDir.exists()) {
            parentDir.mkdir();
        }

        try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(downloadFile))) {
            return this.ftp.retrieveFile(remoteFilePath, outputStream);
        } catch (IOException ex) {
            throw ex;
        }
    }

    public void close() throws IOException {

        if (isConnected()) {

            try {
                ftp.disconnect();
            } catch (IOException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
    }

    /**
     * Check if the ftp is connected (connection is opened)
     *
     * @return
     */
    public boolean isConnected() {
        return null != ftp && ftp.isConnected();
    }

    public boolean isBinaryTransfer() {
        return binaryTransfer;
    }

    public void setBinaryTransfer(boolean binaryTransfer) {
        this.binaryTransfer = binaryTransfer;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public boolean isLocalActive() {
        return localActive;
    }

    public void setLocalActive(boolean localActive) {
        this.localActive = localActive;
    }

    public long getKeepAliveTimeout() {
        return keepAliveTimeout;
    }

    public void setKeepAliveTimeout(long keepAliveTimeout) {
        this.keepAliveTimeout = keepAliveTimeout;
    }

    public int getControlKeepAliveReplyTimeout() {
        return controlKeepAliveReplyTimeout;
    }

    public void setControlKeepAliveReplyTimeout(int controlKeepAliveReplyTimeout) {
        this.controlKeepAliveReplyTimeout = controlKeepAliveReplyTimeout;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getTrustmgr() {
        return trustmgr;
    }

    public void setTrustmgr(String trustmgr) {
        this.trustmgr = trustmgr;
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public void setProxyHost(String proxyHost) {
        this.proxyHost = proxyHost;
    }

    public int getProxyPort() {
        return proxyPort;
    }

    public void setProxyPort(int proxyPort) {
        this.proxyPort = proxyPort;
    }

    public String getProxyUser() {
        return proxyUser;
    }

    public void setProxyUser(String proxyUser) {
        this.proxyUser = proxyUser;
    }

    public String getProxyPassword() {
        return proxyPassword;
    }

    public void setProxyPassword(String proxyPassword) {
        this.proxyPassword = proxyPassword;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isUseEpsvWithIPv4() {
        return useEpsvWithIPv4;
    }

    public void setUseEpsvWithIPv4(boolean useEpsvWithIPv4) {
        this.useEpsvWithIPv4 = useEpsvWithIPv4;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public String getServerType() {
        return serverType;
    }

    public void setServerType(String serverType) {
        this.serverType = serverType;
    }

    public String getRemoteDirectory() {
        return remoteDirectory;
    }

    public void setRemoteDirectory(String remoteDirectory) {
        this.remoteDirectory = remoteDirectory;
    }

    public FTPClient getFtp() {
        return ftp;
    }

    public void setFtp(FTPClient ftp) {
        this.ftp = ftp;
    }

}
