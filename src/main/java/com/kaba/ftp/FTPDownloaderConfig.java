package com.kaba.ftp;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * FTP Downloader supplier
 *
 * @author kaba
 * @since June 1, 2018
 */
@Configuration
public class FTPDownloaderConfig {
    
    @ConditionalOnMissingBean
    @Bean
    public FTPDownloader ftpDownloader(FTPDownloaderProperties props) {
        FTPDownloader fTPDownloader = new FTPDownloader();
        
        fTPDownloader.setHost(props.getHost());
        fTPDownloader.setUsername(props.getUsername());
        
        String passwordPrefixToSkip = props.getPasswordPrefixToSkip();
        String password = props.getPassword().replace(passwordPrefixToSkip, "");
       
        fTPDownloader.setPassword(password);
        fTPDownloader.setPort(props.getPort());
        fTPDownloader.setProxyHost(props.getProxyHost());
        fTPDownloader.setProxyUser(props.getProxyUser());
        fTPDownloader.setProxyPassword(props.getProxyPassword());
        fTPDownloader.setProxyPort(props.getProxyPort());
        fTPDownloader.setEncoding(props.getEncoding());
        fTPDownloader.setServerType(props.getServerType());
        fTPDownloader.setRemoteDirectory(props.getRemoteDirectory());
        fTPDownloader.setTrustmgr(props.getTrustmgr());
        fTPDownloader.setProtocol(props.getProtocol());
        fTPDownloader.setControlKeepAliveReplyTimeout(props.getControlKeepAliveReplyTimeout());
        fTPDownloader.setKeepAliveTimeout(props.getKeepAliveTimeout());
        fTPDownloader.setLocalActive(props.isLocalActive());
        fTPDownloader.setUseEpsvWithIPv4(props.isUseEpsvWithIPv4());
        fTPDownloader.setBinaryTransfer(props.isBinaryTransfer());
        
        return fTPDownloader;

    }

}
