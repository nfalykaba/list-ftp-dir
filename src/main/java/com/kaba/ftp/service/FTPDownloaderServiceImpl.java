package com.kaba.ftp.service;

import com.kaba.ftp.FTPDownloader;
import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import org.springframework.stereotype.Service;

/**
 * Implements FTP Downloader service
 *
 * @author kaba
 * @since June 1, 2018
 */
@Service
public class FTPDownloaderServiceImpl implements FTPDownloaderService {

    @Inject
    FTPDownloader fTPDownloader;

    /**
     * @see FTPDownloaderService.downloadSingleDirectory
     * @param remoteDirName
     * @return downloaded file names
     * @throws IOException 
     */
    @Override
    public List<String> downloadSingleDirectory(String remoteDirName) throws IOException{
        return fTPDownloader.listSingleDirectory(remoteDirName);
    }
}
