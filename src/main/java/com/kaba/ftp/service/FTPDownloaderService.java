package com.kaba.ftp.service;

import java.io.IOException;
import java.util.List;

/**
 * FTP Downloader service
 * @author kaba
 * @since June 1, 2018
 */
public interface FTPDownloaderService {
    /**
     * Download a single directory from a FTP server
     * @param remoteDirName name of the directory to download
     * @return downloaded file names
     * @throws IOException 
     */
    List<String> downloadSingleDirectory(String remoteDirName) throws IOException;
}
