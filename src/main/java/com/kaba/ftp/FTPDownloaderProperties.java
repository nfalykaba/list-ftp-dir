package com.kaba.ftp;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * FTP downloader config
 * @author kaba
 * @since June 1, 2018
 */
@Configuration
@ConfigurationProperties(prefix="ftp.client")
public class FTPDownloaderProperties {

    private boolean binaryTransfer = false;
    private boolean localActive = false;
    private long keepAliveTimeout = -1;
    private int controlKeepAliveReplyTimeout = -1;
    private String protocol; // SSL protocol
    private String trustmgr;

    private String proxyHost;
    private int proxyPort = 80;
    private String proxyUser;
    private String proxyPassword;
    private String passwordPrefixToSkip="SKIP";

    private String host;
    private Integer port = 21;
    private String username;
    private String password;
    private boolean useEpsvWithIPv4 = true;

    private String encoding;
    private String serverType;
    private String remoteDirectory;
    
    public FTPDownloaderProperties(){
        
    }

    public boolean isBinaryTransfer() {
        return binaryTransfer;
    }

    public void setBinaryTransfer(boolean binaryTransfer) {
        this.binaryTransfer = binaryTransfer;
    }

    public boolean isLocalActive() {
        return localActive;
    }

    public void setLocalActive(boolean localActive) {
        this.localActive = localActive;
    }

    public long getKeepAliveTimeout() {
        return keepAliveTimeout;
    }

    public void setKeepAliveTimeout(long keepAliveTimeout) {
        this.keepAliveTimeout = keepAliveTimeout;
    }

    public int getControlKeepAliveReplyTimeout() {
        return controlKeepAliveReplyTimeout;
    }

    public void setControlKeepAliveReplyTimeout(int controlKeepAliveReplyTimeout) {
        this.controlKeepAliveReplyTimeout = controlKeepAliveReplyTimeout;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getTrustmgr() {
        return trustmgr;
    }

    public void setTrustmgr(String trustmgr) {
        this.trustmgr = trustmgr;
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public void setProxyHost(String proxyHost) {
        this.proxyHost = proxyHost;
    }

    public int getProxyPort() {
        return proxyPort;
    }

    public void setProxyPort(int proxyPort) {
        this.proxyPort = proxyPort;
    }

    public String getProxyUser() {
        return proxyUser;
    }

    public void setProxyUser(String proxyUser) {
        this.proxyUser = proxyUser;
    }

    public String getProxyPassword() {
        return proxyPassword;
    }

    public void setProxyPassword(String proxyPassword) {
        this.proxyPassword = proxyPassword;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordPrefixToSkip() {
        return passwordPrefixToSkip;
    }

    public void setPasswordPrefixToSkip(String passwordPrefixToSkip) {
        this.passwordPrefixToSkip = passwordPrefixToSkip;
    }

    public boolean isUseEpsvWithIPv4() {
        return useEpsvWithIPv4;
    }

    public void setUseEpsvWithIPv4(boolean useEpsvWithIPv4) {
        this.useEpsvWithIPv4 = useEpsvWithIPv4;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public String getServerType() {
        return serverType;
    }

    public void setServerType(String serverType) {
        this.serverType = serverType;
    }

    public String getRemoteDirectory() {
        return remoteDirectory;
    }

    public void setRemoteDirectory(String remoteDirectory) {
        this.remoteDirectory = remoteDirectory;
    }
    
}
