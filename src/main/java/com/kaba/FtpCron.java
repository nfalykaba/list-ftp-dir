package com.kaba;

import com.kaba.ftp.service.FTPDownloaderService;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *
 * @author kaban
 */
@Component
@EnableScheduling
public class FtpCron {
     private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(FtpCron.class);
    @Value("${coopec}")
    private Set<String> agences;

    @Value("${ftpStatus}")
    private String outPath;

    @Inject
    private FTPDownloaderService fTPDownloaderService;

    @Scheduled(cron = "${scheduling.cron}")
    public void doMapping() throws Exception {
        
        Path statusPath = Paths.get(outPath);
        try (BufferedWriter writer = Files.newBufferedWriter(statusPath)) {
            agences.stream().forEach(agence -> {
                try {
                    List<String> downloadedFiles = fTPDownloaderService.downloadSingleDirectory(agence);
                    writer.write("------" + agence + " ------:\n");
                    for(String f: downloadedFiles){
                         writer.write(f + ":\n");
                    }
                } catch (IOException ex) {
                    Logger.getLogger(FtpCron.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
        }
        LOGGER.info("-------- END ----");
    }
}
